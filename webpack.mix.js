let mix = require('laravel-mix');

var tailwindcss = require('tailwindcss');
let postcssImport = require('postcss-import');

mix.postCss('assets/css/main.css', 'public/css', [
  postcssImport(),
  tailwindcss('./tailwindconfig.js'),
])
.disableNotifications();
